package com.Nariah.staff;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.Nariah.StaffUtils;
import com.Nariah.utils.Utils;

public class H implements CommandExecutor {

	StaffUtils main;

	public H(StaffUtils plugin) {
		main = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		Player p = (Player) sender;
		if (Utils.mod.contains(p.getUniqueId())) {
			Utils.mod.remove(p.getUniqueId());
			Utils.inVanish.remove(p.getUniqueId());
			
			p.setGameMode(GameMode.SURVIVAL);

			if (Utils.inventory.containsKey(p.getUniqueId())) {
				p.getInventory().setContents(Utils.inventory.get(p.getUniqueId()));

				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						this.main.getConfig().getString("StaffMode.Disabled")));
				p.sendMessage(
						ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Vanish.Disabled")));

				p.getInventory().clear();
				p.getInventory().setLeggings(null);
				p.performCommand("spawn");

				for (Player players : Bukkit.getOnlinePlayers()) {
						players.showPlayer(p);
				}

			}

		} else {
			Utils.mod.add(p.getUniqueId());
			Utils.inVanish.add(p.getUniqueId());
			
			p.setGameMode(GameMode.CREATIVE);
			p.getInventory().clear();
			Utils.inventory.put(p.getUniqueId(), p.getInventory().getContents());
			Items.staffItems(p);

			p.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("StaffMode.Enabled")));
			p.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Vanish.Enabled")));

			for (Player players : Bukkit.getOnlinePlayers()) {
					players.hidePlayer(p);
			}

			for (Player staff : Bukkit.getOnlinePlayers()) {
				if (staff.hasPermission("StaffUtils.staff")) {
					staff.showPlayer(p);
				}
			}

		}

		return true;
	}

}
