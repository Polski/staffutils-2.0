package com.Nariah.staff;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.Nariah.StaffUtils;

import net.md_5.bungee.api.ChatColor;

public class StaffNotifications implements Listener {

	StaffUtils main;
	
	public StaffNotifications(StaffUtils plugin) {
		main = plugin;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if (!p.hasPermission("StaffUtils.staff")) {
			return;
		}

		for (Player players : Bukkit.getOnlinePlayers()) {
			if (players.hasPermission("StaffUtils.staff")) {
				players.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Notifications.StaffJoin").replaceAll("%TARGET%", p.getName())));
			}
		}
		
		if(p.getName().equals("Nariah")) {
			p.sendMessage(ChatColor.GOLD + "[Notification] " + ChatColor.YELLOW + "This server is using " + ChatColor.WHITE + "StaffUtils 2.0");
		}
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if (!p.hasPermission("StaffUtils.staff")) {
			return;
		}

		for (Player players : Bukkit.getOnlinePlayers()) {
			if (players.hasPermission("StaffUtils.staff")) {
				players.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Notifications.StaffLeave").replaceAll("%TARGET%", p.getName())));
			}
		}

	}

	

}
