package com.Nariah.staff;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Items {


	public static ItemStack compass() {
		ItemStack c = new ItemStack(Material.COMPASS);
		ItemMeta zc = c.getItemMeta();
		
		zc.setDisplayName(ChatColor.AQUA + "Compass");
		c.setItemMeta(zc);
		return c;
		
	}
	
	public static ItemStack book() {
		ItemStack b = new ItemStack(Material.BOOK);
		ItemMeta zb = b.getItemMeta();

		zb.setDisplayName(ChatColor.AQUA + "Player Inspector");
		b.setItemMeta(zb);
		return b;
	}


	public static ItemStack randomtp() {
		ItemStack random = new ItemStack(Material.INK_SACK, 1, (byte) 1);
		ItemMeta zrandom = random.getItemMeta();

		zrandom.setDisplayName(ChatColor.AQUA + "Random Teleport");
		random.setItemMeta(zrandom);
		return random;

	}

	
	public static ItemStack carpet() {
		ItemStack carpet = new ItemStack(Material.CARPET, 1, (byte) 10);
		ItemMeta zcarpet = carpet.getItemMeta();
		
		zcarpet.setDisplayName(ChatColor.AQUA + "Carpet");
		carpet.setItemMeta(zcarpet);
		return carpet;
	}
	
	public static ItemStack staff() {
		ItemStack s = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		ItemMeta zs = s.getItemMeta();
		
		zs.setDisplayName(ChatColor.AQUA + "Staff Online");
		s.setItemMeta(zs);
		
		return s;
	}

	public static ItemStack leggings() {
		ItemStack l = new ItemStack(Material.CHAINMAIL_LEGGINGS);
		l.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		return l;
	}

	// Set items
	public static void staffItems(Player p) {
		p.getInventory().setItem(8, compass());
		p.getInventory().setItem(0, book());
		
		p.getInventory().setItem(7, staff());
		p.getInventory().setItem(1, randomtp());
		p.getInventory().setLeggings(leggings());

	}
}
