package com.Nariah.staff;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class StaffChat implements CommandExecutor {

	StaffUtils main;

	public StaffChat(StaffUtils plugin) {
		main = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /sc {message}");
			return true;
		}
		
		StringBuilder message = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			message.append(args[i] + " ");
		}
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.hasPermission("StaffUtils.staff")) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("StaffChat.Format").replaceAll("%SENDER%", sender.getName()).replace("%MESSAGE%", message)));
			}
		}
		return true;
	}
	
	
}
