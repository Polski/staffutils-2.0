package com.Nariah.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.inventory.ItemStack;

public class Utils {

	
	//MUTECHAT
	public static boolean chat = true;
	
	//VANISH
	public static HashSet<UUID> inVanish = new HashSet<>();
	
	//FREEZE 
	public static HashSet<UUID> frozen = new HashSet<>();
	public static HashSet<UUID> frozenAll = new HashSet<>();
	
	//PANIC
	public static HashSet<UUID> inPanic = new HashSet<>();
	
	//BLACKLIST
	public static HashSet<UUID> blackListed = new HashSet<>();
	
	//MOD
	public static ConcurrentHashMap<UUID, ItemStack[]> inventory = new ConcurrentHashMap<>();
	public static HashSet<UUID> mod = new HashSet<>();
	
	//STAFF /list /online staff
	public static ArrayList<String> staff = new ArrayList<>();
	
	//LOGINB
	public static HashSet<UUID> loggedIn = new HashSet<>();
}
