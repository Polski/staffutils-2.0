package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Report implements CommandExecutor {

	StaffUtils main;

	public Report(StaffUtils plugin) {
		main = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (args.length < 2) {
			sender.sendMessage(ChatColor.RED + "Usage: /report {player} {reason}");
			return true;
		}

		if (args.length >= 2) {
			Player target = Bukkit.getServer().getPlayer(args[0]);

			if (target == null) {
				sender.sendMessage(ChatColor
						.translateAlternateColorCodes('&', this.main.getConfig().getString("TargetNotFound.Message"))
						.replaceAll("%TARGET%", args[0]));
				return true;
			}

			if (target == sender) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Report.ReportYourself")));
				return true;
			}

			StringBuilder message = new StringBuilder();
			for (int i = 1; i < args.length; i++) {
				message.append(args[i] + " ");
			}

			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Report.SentMessage")));
			for (Player staff : Bukkit.getOnlinePlayers()) {
				if (staff.hasPermission("StaffUtils.staff")) {
					staff.sendMessage(ChatColor.translateAlternateColorCodes('&',
							this.main.getConfig().getString("Report.Format").replaceAll("%SENDER%", sender.getName())
									.replaceAll("%TARGET%", target.getName()).replace("%MESSAGE%", message)));
				}
			}
		}

		return true;
	}
}
