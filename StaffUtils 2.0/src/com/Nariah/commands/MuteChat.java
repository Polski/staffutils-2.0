package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

import com.Nariah.StaffUtils;
import com.Nariah.utils.Utils;

public class MuteChat implements CommandExecutor, Listener {

	StaffUtils main;

	public MuteChat(StaffUtils plugin) {
		main = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}
		
		if (Utils.chat) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("MuteChat.Locked").replaceAll("%SENDER%", sender.getName())));
			Utils.chat = false;

		} else {
			Utils.chat = true;
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("MuteChat.Unlocked").replaceAll("%SENDER%", sender.getName())));
		}
		return true;

	}
	

	
}
