package com.Nariah.commands.essentials;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class TpHere implements CommandExecutor {

	StaffUtils main;

	public TpHere(StaffUtils plugin) {
		main = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (!sender.hasPermission("StaffUtils.staff.advanced")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}
		
		if(args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /tphere {player}");
			return true;
		}
		
		if(args.length == 1) {
			Player target = Bukkit.getPlayer(args[0]);
			Player p = (Player) sender;
			
			if (target == null) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
						this.main.getConfig().getString("TargetNotFound.Message")).replaceAll("%TARGET%", args[0]));
				return true;
			}
			
			if(target == sender) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("TpHere.TeleportYourself")));
				return true;
			}
			
			target.teleport(p.getLocation());
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("TpHere.TpHereMessage").replaceAll("%TARGET%", target.getName())));
			
			
		}
		
		return true;
	}
	
}
