package com.Nariah.commands.essentials;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Feed implements CommandExecutor {

	StaffUtils main;

	public Feed(StaffUtils plugin) {
		main = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /feed {player}");
			return true;
		}

		if (args.length == 1) {
			Player target = Bukkit.getPlayer(args[0]);

			if (target == null) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
						this.main.getConfig().getString("TargetNotFound.Message")).replaceAll("%TARGET%", args[0]));
				return true;
			}

			if (target.getGameMode().equals(GameMode.SURVIVAL)) {
				((CraftPlayer) target).setSaturation((float) 20.0);
					target.sendMessage(
							ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Feed.TargetMessage")));
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
							this.main.getConfig().getString("Feed.FeedMessage").replaceAll("%TARGET%", target.getName())));
			}
		}
		return true;

	}
}
