package com.Nariah.commands.essentials;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Teleport implements CommandExecutor {

	StaffUtils main;

	public Teleport(StaffUtils plugin) {
		main = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /tp {player}");
			return true;
		}

		if (args.length == 1) {
			Player p = (Player) sender;
			Player target = Bukkit.getPlayer(args[0]);

			if (target == null) {
				sender.sendMessage(ChatColor
						.translateAlternateColorCodes('&', this.main.getConfig().getString("TargetNotFound.Message"))
						.replaceAll("%TARGET%", args[0]));
				return true;
			}

			if (target == sender) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Teleport.TeleportYourself")));
				return true;
			}

			p.teleport(target);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Teleport.TeleportMessage").replaceAll("%TARGET%", target.getName())));

		}

		return true;
	}
}
