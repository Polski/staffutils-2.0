package com.Nariah.commands.essentials;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Gamemode implements CommandExecutor {

	StaffUtils main;

	public Gamemode(StaffUtils plugin) {
		main = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}
		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /gamemode {creative/survival}");
			return true;
		}

		if (args.length == 1) {
			Player p = (Player) sender;

			if (args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("1")) {
				if (p.getGameMode().equals(GameMode.SURVIVAL)) {

					p.setGameMode(GameMode.CREATIVE);
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							this.main.getConfig().getString("Gamemode.Creative")));
				} else {
					p.sendMessage(ChatColor.RED + "You are already in creative.");
				}
			} else if (args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("0")) {
				if (p.getGameMode().equals(GameMode.CREATIVE)) {
					p.setGameMode(GameMode.SURVIVAL);
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							this.main.getConfig().getString("Gamemode.Survival")));
				} else {
					p.sendMessage(ChatColor.RED + "You are already in survival.");
				}
			}
		}

		return true;
	}

}
