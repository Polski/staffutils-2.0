package com.Nariah.commands.essentials;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class SetSpawn implements CommandExecutor {

	StaffUtils main;

	public SetSpawn(StaffUtils plugin) {
		main = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (!sender.hasPermission("StaffUtils.staff.advanced")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}
		if (sender.hasPermission("StaffUtils.staff.advanced")) {
			Player p = (Player) sender;
			Location l = p.getLocation();

			int xc = l.getBlockX();
			int yc = l.getBlockY();
			int zc = l.getBlockZ();
			p.getWorld().setSpawnLocation(xc, yc, zc);
			p.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("SetSpawn.SetSpawnMessage")));
		}
		return true;
	}
}
