package com.Nariah.commands.essentials;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;
import com.Nariah.utils.Utils;

public class Vanish implements CommandExecutor {

	StaffUtils main;

	public Vanish(StaffUtils plugin) {
		main = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}
		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (args.length == 0) {
			Player p = (Player) sender;
			if (Utils.inVanish.contains(p.getUniqueId())) {
				Utils.inVanish.remove(p.getUniqueId());
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Vanish.Disabled")));

				
				for (Player players : Bukkit.getOnlinePlayers()) {
					players.showPlayer(p);
					
				}
			} else {
				Utils.inVanish.add(p.getUniqueId());
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Vanish.Enabled")));
				
				for (Player players : Bukkit.getOnlinePlayers()) {
					players.hidePlayer(p);
					
					if(players.hasPermission("StaffUtils.staff")) {
						players.showPlayer(p);
					}
				}
			}
		}
		return true;
	}

}
