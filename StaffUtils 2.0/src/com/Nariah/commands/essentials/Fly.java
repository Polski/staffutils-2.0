package com.Nariah.commands.essentials;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Fly implements CommandExecutor {

	StaffUtils main;

	public Fly(StaffUtils plugin) {
		main = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /fly {on/off}");
			return true;
		}
		if (args.length == 1) {
			Player p = (Player) sender;
			if (p.getGameMode().equals(GameMode.SURVIVAL)) {
				if (sender.hasPermission("StaffUtils.staff")) {

					if (args[0].equalsIgnoreCase("on")) {
						p.setAllowFlight(true);
						p.sendMessage(
								ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Fly.on")));
					} else if (args[0].equalsIgnoreCase("off")) {
						p.setAllowFlight(false);
						p.sendMessage(
								ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Fly.off")));
					}
				}

			} else {
				p.sendMessage(ChatColor.RED + "You are already in creative.");
			}
		}
		return true;
	}
}
