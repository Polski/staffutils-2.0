package com.Nariah.commands.essentials;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Heal implements CommandExecutor {

	StaffUtils main;

	public Heal(StaffUtils plugin) {
		main = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /heal {player}");
			return true;
		}

		if (args.length == 1) {
			Player target = Bukkit.getPlayer(args[0]);
			Player p = (Player) sender;

			if (target == null) {
				sender.sendMessage(ChatColor
						.translateAlternateColorCodes('&', this.main.getConfig().getString("TargetNotFound.Message"))
						.replaceAll("%TARGET%", args[0]));
				return true;
			}

			if (p.getGameMode().equals(GameMode.SURVIVAL)) {
				target.setHealth(20D);
				target.sendMessage(
						ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Heal.TargetMessage")));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Heal.HealMessage").replaceAll("%TARGET%", args[0])));
			}
		}

		return true;
	}
}
