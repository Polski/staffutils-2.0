package com.Nariah.commands.essentials;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Spawn implements CommandExecutor {

	StaffUtils main;

	public Spawn(StaffUtils plugin) {
		main = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = (Player) sender;

		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}
		if (sender.hasPermission("StaffUtils.staff")) {
			p.teleport(p.getWorld().getSpawnLocation());
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Spawn.SpawnMessage")));
		}

		return true;
	}
}
