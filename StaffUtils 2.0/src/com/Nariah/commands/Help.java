package com.Nariah.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Help implements CommandExecutor {

	StaffUtils main;

	public Help(StaffUtils staff) {

		main = staff;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		Player p = (Player) sender;

		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Spacer1")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line1")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line2")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line3")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', " "));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line5")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line6")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line7")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', " "));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line9")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line10")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line11")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Line12")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Spacer2")));
		return true;

	}
}
