package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;
import com.Nariah.utils.Utils;

public class FreezeAll implements CommandExecutor {
	
	StaffUtils main;

	public FreezeAll(StaffUtils staff) {

		main = staff;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("StaffUtils.staff.advanced")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		for (Player players : Bukkit.getOnlinePlayers()) {
			if (Utils.frozenAll.contains(players.getUniqueId())) {
				Utils.frozenAll.remove(players.getUniqueId());
				players.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("FreezeAll.InActive")));
			} else {
				if (!Utils.frozenAll.contains(players.getUniqueId())) {
					Utils.frozenAll.add(players.getUniqueId());
					players.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("FreezeAll.Active")));
				}

			}
		}
		return true;

	}
}
