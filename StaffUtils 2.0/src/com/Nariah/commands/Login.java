package com.Nariah.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import com.Nariah.StaffUtils;
import com.Nariah.utils.Utils;

import net.md_5.bungee.api.ChatColor;

public class Login implements CommandExecutor {

	StaffUtils main;

	public Login(StaffUtils plugin) {
		main = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}
		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (this.main.getConfig().getBoolean("Login.Enabled", false)) {
			sender.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Login.Disabled")));
			return true;
		}

		Player p = (Player) sender;
		if (!Utils.loggedIn.contains(p.getUniqueId())) {
			p.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Login.AlreadyLoggedIn")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /login {pin}");
			return true;
		}
		if (args.length == 1) {
			if (args[0].equals(main.getConfig().getString("Login.PIN"))) {
				if (Utils.loggedIn.contains(p.getUniqueId())) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							main.getConfig().getString("Login.Successful")));
					
					Utils.loggedIn.remove(p.getUniqueId());
					
					for (PotionEffect effect : p.getActivePotionEffects()) {
				        p.removePotionEffect(effect.getType());
					}
				}
			} else {
				p.sendMessage(
						ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Login.WrongPIN")));
			}

		}

		return true;
	}

}
