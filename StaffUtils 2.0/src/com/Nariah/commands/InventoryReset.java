package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import com.Nariah.StaffUtils;

public class InventoryReset implements CommandExecutor, Listener {

	StaffUtils main;

	public InventoryReset(StaffUtils plugin) {
		main = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /recover {player}");
			return true;
		}

		if (args.length == 1) {

			Player target = Bukkit.getPlayer(args[0]);

			if (target == null) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
						this.main.getConfig().getString("TargetNotFound.Message")).replaceAll("%TARGET%", args[0]));
				return true;
			}
			
			StaffUtils.getPlugin();
			if (!StaffUtils.inventoryStorage.containsKey(target.getUniqueId())) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("InventoryReset.Failed")));
			} else {
				StaffUtils.getPlugin();
				if (StaffUtils.inventoryStorage.containsKey(target.getUniqueId())) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("InventoryReset.RecoverMessage")));
					StaffUtils.getPlugin();
					target.getInventory().setContents(
							(ItemStack[]) StaffUtils.inventoryStorage.get(target.getUniqueId()));
					StaffUtils.getPlugin();
					target.getInventory().setArmorContents(
							(ItemStack[]) StaffUtils.armourStorage.get(target.getUniqueId()));
					StaffUtils.getPlugin();
					StaffUtils.inventoryStorage.remove(target.getUniqueId());
					StaffUtils.getPlugin();
					StaffUtils.armourStorage.remove(target.getUniqueId());
				}
			}

		}

		return true;
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player p = e.getEntity();

		StaffUtils.armourStorage.put(p.getUniqueId(), p.getInventory().getArmorContents());
		StaffUtils.inventoryStorage.put(p.getUniqueId(), p.getInventory().getContents());

	}

}