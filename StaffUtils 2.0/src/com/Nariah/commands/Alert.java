package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.Nariah.StaffUtils;

public class Alert implements CommandExecutor {

	StaffUtils main;

	public Alert(StaffUtils plugin) {
		main = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("StaffUtils.staff.advanced")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /alert {message}");
			return true;
		}
		
		StringBuilder message = new StringBuilder();
		for (int j = 0; j < args.length; j++) {
			message.append(args[j] + " ");
		}

		String msg = message.toString();
		msg = msg.replaceAll("&","§");

		if (args.length >= 1) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Alert.Prefix") + ChatColor.WHITE + msg));
		}
		return true;

	}
}
