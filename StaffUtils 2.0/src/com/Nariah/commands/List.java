package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;
import com.Nariah.utils.Utils;

import net.md_5.bungee.api.ChatColor;

public class List implements CommandExecutor {

	StaffUtils main;

	public List(StaffUtils plugin) {
		main = plugin;
	}

	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (sender.hasPermission("StaffUtils.staff")) {
			Player p = (Player) sender;
			// Remove on command
			Utils.staff.remove(p.getName());
			// Re-add
			Utils.staff.add(p.getName());
		}
		Player p = (Player) sender;
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("List.Spacer1")));
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("List.Line1") + Bukkit.getOnlinePlayers().length));

		if (Utils.staff.size() == 0) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("List.Line2")));

		} else {
			{
				if (Utils.staff.size() != 0) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
							this.main.getConfig().getString("List.Line3").replaceAll("%STAFFMEMBERS%",
									Utils.staff.toString().replace('[', ' ').replace(']', ' '))));

				}

			}
		}
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Help.Spacer2")));
		return true;

	}
}