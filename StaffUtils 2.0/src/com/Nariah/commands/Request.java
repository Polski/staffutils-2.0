package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Request implements CommandExecutor {

	StaffUtils main;

	public Request(StaffUtils plugin) {
		main = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /request {message}");
			return true;
		}

		if (this.main.getConfig().getBoolean("Request.Enabled", false)) {
			sender.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Request.Disabled")));
			return true;
		}

		StringBuilder message = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			message.append(args[i] + " ");
		}

		if (args.length == 1) {
			for (Player players : Bukkit.getOnlinePlayers()) {
				if (players.hasPermission("StaffUtils.staff")) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
							this.main.getConfig().getString("Request.SentRequest")));
					players.sendMessage(ChatColor.translateAlternateColorCodes('&',
							this.main.getConfig().getString("Request.Format").replaceAll("%SENDER%", sender.getName())
									.replace("%MESSAGE%", message)));
				}
			}

		}

		return true;
	}
}
