package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class Helpop implements CommandExecutor {

	StaffUtils main;

	public Helpop(StaffUtils plugin) {
		main = plugin;
	}
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}
		
		if(args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /helpop {message}");
			return true;
		}
		
		StringBuilder message = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			message.append(args[i] + " ");
		}

		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Helpop.SentMessage")));
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.hasPermission("StaffUtils.staff")) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Helpop.Format").replaceAll("%SENDER%", sender.getName()).replace("%MESSAGE%", message)));
			}

		}
		return true;
	}
}
