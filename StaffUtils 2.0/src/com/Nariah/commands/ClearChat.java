package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.Nariah.StaffUtils;

public class ClearChat implements CommandExecutor {

	StaffUtils main;

	public ClearChat(StaffUtils plugin) {
		main = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		for (int i = 0; i < 225; i++) {
			Bukkit.broadcastMessage(" ");
		}
		
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("ClearChat.Message").replaceAll("%SENDER%", sender.getName())));

		return true;

	}
}
