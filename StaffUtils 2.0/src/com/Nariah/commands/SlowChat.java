package com.Nariah.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;

import com.Nariah.StaffUtils;

public class SlowChat implements CommandExecutor, Listener {

	StaffUtils main;

	public SlowChat(StaffUtils plugin) {
		main = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
		if (!sender.hasPermission("StaffUtils.staff.advanced")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}
		if (args.length < 1) {
			sender.sendMessage(ChatColor.GREEN + "Current Slowchat Delay: " + ChatColor.GREEN
					+ String.valueOf(this.main.getConfig().getInt("SlowChat.interval")) + " seconds");
			return true;
		}
		try {
			int newint = Integer.parseInt(args[0]);

			this.main.getConfig().set("SlowChat.interval", Integer.valueOf(newint));
			this.main.saveConfig();

			if (newint == 0) {
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("SlowChat.NoLongerSlowed")));
				return true;
			}
			sender.sendMessage(ChatColor.GREEN + "Updated time between messages to " + args[0] + ChatColor.GREEN
					+ " second" + (newint == 1 ? "" : "s"));
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("SlowChat.Broadcast")).replaceAll("%SENDER%", sender.getName()));

		} catch (NumberFormatException e) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("SlowChat.ValidNumber").replaceAll("%NUMBER%", args[0])));
		}
		return true;
	}

	private final Map<String, Long> times = new HashMap<>();

	@EventHandler
	public void onPlayerChat(PlayerChatEvent e) {
		if (e.getPlayer().hasPermission("Slowchat.bypass")) {
			return;
		}
		long newint = System.currentTimeMillis();
		String name = e.getPlayer().getName();
		Long lastChat = (Long) times.get(e.getPlayer().getName());
		if (lastChat != null) {
			long earliestNext = lastChat.longValue() + this.main.getConfig().getInt("SlowChat.interval") * 1000;
			if (newint < earliestNext) {
				int timeRemaining = (int) ((earliestNext - newint) / 1000L) + 1;
				e.getPlayer().sendMessage(ChatColor.RED + "You can not talk for another " + ChatColor.YELLOW
						+ timeRemaining + ChatColor.RED + " second" + (timeRemaining > 1 ? "s" : ""));
				
				e.setCancelled(true);
				return;
			}
		}
		times.put(name, Long.valueOf(newint));
	}

}
