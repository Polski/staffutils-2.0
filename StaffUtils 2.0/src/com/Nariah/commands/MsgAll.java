package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.StaffUtils;

public class MsgAll implements CommandExecutor {

	StaffUtils main;

	public MsgAll(StaffUtils plugin) {
		main = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("SenderInstanceOfPlayer.Message")));
			return true;
		}

		if (!sender.hasPermission("StaffUtils.staff.advanced")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /msgall {message}");
			return true;
		}

		StringBuilder message = new StringBuilder();
		for (int j = 0; j < args.length; j++) {
			message.append(args[j] + " ");
		}
		
		String msg = message.toString();
		msg = msg.replaceAll("&", "�");

		if (args.length >= 1) {
			Player p = (Player) sender;

			for (Player players : Bukkit.getOnlinePlayers()) {
				players.playSound(players.getLocation(), Sound.ORB_PICKUP, 1F, 1F);
				players.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("MsgAll.Format").replaceAll("%SENDER%", p.getDisplayName()).replaceAll("%MESSAGE%", msg)));
			}

		}
		return true;
	}
}
