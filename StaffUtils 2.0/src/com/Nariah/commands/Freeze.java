package com.Nariah.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.Nariah.StaffUtils;
import com.Nariah.utils.Utils;

public class Freeze implements CommandExecutor, Listener {

	StaffUtils main;

	public Freeze(StaffUtils staff) {

		main = staff;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission("StaffUtils.staff")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Permission.NoPermission")));
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /freeze {player}");
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);

		if (target == sender) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Freeze.FreezeYourself")));
			return true;

		}
		if (target == null) {
			sender.sendMessage(ChatColor
					.translateAlternateColorCodes('&', this.main.getConfig().getString("TargetNotFound.Message"))
					.replaceAll("%TARGET%", args[0]));
			return true;
		}

		if (Utils.frozen.contains(target.getUniqueId())) {
			Utils.frozen.remove(target.getUniqueId());
			target.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Unfreeze.TargetMessage")));
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig()
					.getString("Unfreeze.SenderMessage").replaceAll("%TARGET%", target.getName())));
		} else {
			Utils.frozen.add(target.getUniqueId());
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					this.main.getConfig().getString("Freeze.SenderMessage").replaceAll("%TARGET%", target.getName())));

			this.main.getServer().getScheduler().scheduleAsyncRepeatingTask(this.main, new Runnable() {
				public void run() {
					if (Utils.frozen.contains(target.getUniqueId())) {
						target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f████&c█&f████"));
						target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f███&c█&6█&c█&f███"));
						target.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"&f██&c█&6█&0█&6█&c█&f██" + "     &c&lYou have been frozen."));
						target.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"&f██&c█&6█&0█&6█&c█&f██" + "     &4&lLogging  out will result in a ban."));
						target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f█&c█&6██&0█&6██&c█&f█"
								+ "     &e&lPlease join " + main.getConfig().getString("Help.Teamspeak")));
						target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f█&c█&6█████&c█&f█"));
						target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c█&6███&0█&6███&c█"));
						target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c█████████"));
					}
				}
			}, 60L, 200L);
		}
		return true;
	}
}
