package com.Nariah.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.Nariah.utils.Utils;

public class FreezeListener implements Listener {

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if (Utils.frozen.contains(p.getUniqueId())) {
			for (Player p1 : Bukkit.getOnlinePlayers()) {
				if (p1.hasPermission("StaffUtils.staff")) {
					p1.sendMessage(" ");
					p1.sendMessage(ChatColor.DARK_RED + "" + p.getName() + " has logged out while frozen!");
					p1.sendMessage(" ");
				}
			}
		}

	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if (Utils.frozen.contains(p.getUniqueId()) || Utils.frozenAll.contains(p.getUniqueId())) {
			p.sendMessage(ChatColor.RED + "You can not do this!");
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		if (Utils.frozen.contains(p.getUniqueId()) || Utils.frozenAll.contains(p.getUniqueId())) {
			if (!p.hasPermission("StaffUtils.staff") && e.getMessage().startsWith("/")) {
				e.setCancelled(true);
				p.sendMessage(ChatColor.RED + "You can not do this!");
				return;
			} else if (e.getMessage().startsWith("/reply") || e.getMessage().startsWith("/r")) {
				e.setCancelled(false);
			}
		}
	}



	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if (Utils.frozen.contains(p.getUniqueId()) || Utils.frozenAll.contains(p.getUniqueId())) {
			e.setTo(e.getFrom());
		}
	}

	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		if (Utils.frozen.contains(p.getUniqueId()) || Utils.frozenAll.contains(p.getUniqueId())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		if (Utils.frozen.contains(p.getUniqueId()) || Utils.frozenAll.contains(p.getName())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onHit(EntityDamageByEntityEvent e) {
		Entity e1 = e.getEntity();
		Entity e2 = e.getDamager();

		if (e1 instanceof Player && e2 instanceof Player) {
			Player damager = (Player) e2;

			if (Utils.frozen.contains(damager.getUniqueId()) || Utils.frozenAll.contains(damager.getUniqueId())) {
				e.setCancelled(true);
			}
		}

	}

}
