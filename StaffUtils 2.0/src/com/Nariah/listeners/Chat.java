package com.Nariah.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.Nariah.StaffUtils;
import com.Nariah.utils.Utils;

public class Chat implements Listener {

	StaffUtils main;

	public Chat(StaffUtils plugin) {
		plugin = main;
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		if (!Utils.chat && !p.hasPermission("StaffUtils.staff")) {
			p.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("MuteChat.OnChat")));
			e.setCancelled(true);

		} else if (p.hasPermission("StaffUtils.staff")) {
			e.setCancelled(false);
		}

	}
}
