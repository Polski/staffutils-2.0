package com.Nariah.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import com.Nariah.StaffUtils;
import com.Nariah.staff.Items;
import com.Nariah.utils.Utils;

public class Join implements Listener {

	StaffUtils main;

	public Join(StaffUtils plugin) {
		main = plugin;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {

		Player p = e.getPlayer();

		if (p.hasPermission("StaffUtils.staff")) {
			
			// ADD PLAYER TO HASHBROWNS
			Utils.mod.add(p.getUniqueId());
			Utils.staff.add(p.getName());
			Utils.inVanish.add(p.getUniqueId());

			p.getInventory().clear();
			p.getInventory().setLeggings(null);
			Utils.inventory.put(p.getUniqueId(), p.getInventory().getContents());

			Items.staffItems(p);
			p.setGameMode(GameMode.CREATIVE);

			p.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("StaffMode.Enabled")));
			p.sendMessage(
					ChatColor.translateAlternateColorCodes('&', this.main.getConfig().getString("Vanish.Enabled")));

			for (Player players : Bukkit.getOnlinePlayers()) {
				players.hidePlayer(p);

				if (players.hasPermission("StaffUtils.staff")) {
					players.showPlayer(p);
				}
			}
		}
	}

}
