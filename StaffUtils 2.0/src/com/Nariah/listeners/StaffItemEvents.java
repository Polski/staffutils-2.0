package com.Nariah.listeners;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import com.Nariah.StaffUtils;
import com.Nariah.staff.Items;
import com.Nariah.utils.Utils;

public class StaffItemEvents implements Listener {

	StaffUtils main;

	public StaffItemEvents(StaffUtils plugin) {
		main = plugin;
	}

	@EventHandler
	public void onInteract(PlayerInteractEntityEvent e) {
		Player target = (Player) e.getRightClicked();
		Player p = e.getPlayer();

		if (p.getItemInHand().getType().equals(Material.BOOK) && e.getRightClicked() instanceof Player) {
			if (p.hasPermission("StaffUtils.staff")) {
				p.openInventory(target.getInventory());
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig()
						.getString("StaffMode.OpenInventory").replaceAll("%TARGET%", target.getName())));
			}
		}
	}

	@EventHandler
	public void onRandom(PlayerInteractEvent e) {
		Player p = e.getPlayer();

		if (p.getItemInHand().getType().equals(Material.INK_SACK)) {
			if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
				int random = new Random().nextInt(Bukkit.getOnlinePlayers().length);
				Player newplayer = (Player) Bukkit.getOnlinePlayers().clone()[random];

				if (p.equals(newplayer)) {
					random = new Random().nextInt(Bukkit.getOnlinePlayers().length);
					newplayer = (Player) Bukkit.getOnlinePlayers().clone()[random];
				}
				// TP
				if (p.hasPermission("StaffUtils.staff")) {
					p.teleport(newplayer);
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.main.getConfig()
							.getString("StaffMode.RandomTP").replaceAll("%TARGET%", newplayer.getName())));
				}
			}
		} else if (p.getItemInHand().equals(Items.staff())) {
			if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
				if (p.hasPermission("StaffUtils.staff")) {   
					Utils.staff.remove(p.getName());
					
					Utils.staff.add(p.getName());
					
					p.sendMessage(ChatColor.AQUA + "Staff Online: ");
					p.sendMessage(ChatColor.BLUE + "* " + ChatColor.AQUA + Utils.staff.toString().replace('[', ' ').replace(']', ' '));
					
					for(Player players : Bukkit.getOnlinePlayers()) {
						if(players.hasPermission("StaffUtils.staff")) {
							
						}
					}
				}
			}
		}

	}

	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		if (Utils.mod.contains(e.getWhoClicked().getUniqueId())) {
			e.setCancelled(true);
		}

	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		if (Utils.mod.contains(e.getPlayer().getUniqueId())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		if (Utils.mod.contains(e.getPlayer().getUniqueId())) {
			e.setCancelled(true);

		}
	}

	@EventHandler
	public void onPickUp(PlayerPickupItemEvent e) {
		if (Utils.mod.contains(e.getPlayer().getUniqueId())) {
			e.setCancelled(true);

		}
	}

	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		if (Utils.mod.contains(e.getPlayer().getUniqueId())) {

			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onHit(EntityDamageByEntityEvent e) {
		Entity e1 = e.getEntity();
		Entity e2 = e.getDamager();

		if (e1 instanceof Player && e2 instanceof Player) {
			Player damager = (Player) e2;

			if (Utils.mod.contains(damager.getUniqueId())) {
				e.setCancelled(true);
			}
		}

	}

}
