package com.Nariah.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.Nariah.StaffUtils;
import com.Nariah.utils.Utils;

public class LoginListener implements Listener {

	StaffUtils main;

	public LoginListener(StaffUtils plugin) {
		main = plugin;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();

		if (p.hasPermission("StaffUtils.staff")) {

			if (this.main.getConfig().getBoolean("Login.Enabled", true)) {
				p.sendMessage(ChatColor.RED + "Please login using /login {pin}");
				p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE, 1, true));

				Utils.loggedIn.add(p.getUniqueId());
			} else {
				for (PotionEffect effect : p.getActivePotionEffects()) {
					p.removePotionEffect(effect.getType());
				}
			}

			// LOGIN RUNNABLE
			this.main.getServer().getScheduler().scheduleAsyncRepeatingTask(this.main, new Runnable() {
				public void run() {
					if (Utils.loggedIn.contains(p.getUniqueId())) {
						if (main.getConfig().getBoolean("Login,Enabled", true)) {
							p.sendMessage(ChatColor.RED + "Please login using /login {pin}");
						}
					}
				}
			}, 60L, 100L);
		}
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if (Utils.loggedIn.contains(p.getUniqueId())) {
			p.sendMessage(ChatColor.RED + "You can not do this!");
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		if (Utils.loggedIn.contains(p.getUniqueId())) {
			if (p.hasPermission("StaffUtils.staff") && e.getMessage().startsWith("/")) {
				e.setCancelled(true);

			}
			if (p.hasPermission("StaffUtils.staff") && e.getMessage().startsWith("/login")) {
				e.setCancelled(false);
			}
		}
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if (Utils.loggedIn.contains(p.getUniqueId())) {
			e.setTo(e.getFrom());
		}
	}

	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		if (Utils.loggedIn.contains(p.getUniqueId())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		if (Utils.loggedIn.contains(p.getUniqueId())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onHit(EntityDamageByEntityEvent e) {
		Entity e1 = e.getEntity();
		Entity e2 = e.getDamager();

		if (e1 instanceof Player && e2 instanceof Player) {
			Player damager = (Player) e2;

			if (Utils.loggedIn.contains(damager.getUniqueId())) {
				e.setCancelled(true);
			}
		}

	}

}
