package com.Nariah;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.Nariah.commands.Alert;
import com.Nariah.commands.ClearChat;
import com.Nariah.commands.Freeze;
import com.Nariah.commands.FreezeAll;
import com.Nariah.commands.Help;
import com.Nariah.commands.Helpop;
import com.Nariah.commands.InventoryReset;
import com.Nariah.commands.List;
import com.Nariah.commands.Login;
import com.Nariah.commands.MsgAll;
import com.Nariah.commands.MuteChat;
import com.Nariah.commands.Report;
import com.Nariah.commands.Request;
import com.Nariah.commands.SlowChat;
import com.Nariah.commands.essentials.Feed;
import com.Nariah.commands.essentials.Fly;
import com.Nariah.commands.essentials.Gamemode;
import com.Nariah.commands.essentials.Heal;
import com.Nariah.commands.essentials.Kill;
import com.Nariah.commands.essentials.SetSpawn;
import com.Nariah.commands.essentials.Spawn;
import com.Nariah.commands.essentials.Teleport;
import com.Nariah.commands.essentials.TpHere;
import com.Nariah.commands.essentials.Vanish;
import com.Nariah.listeners.Chat;
import com.Nariah.listeners.FreezeListener;
import com.Nariah.listeners.Join;
import com.Nariah.listeners.LoginListener;
import com.Nariah.listeners.StaffItemEvents;
import com.Nariah.staff.H;
import com.Nariah.staff.StaffChat;
import com.Nariah.staff.StaffNotifications;

/*
 * Created By Nariah xox
 */
public class StaffUtils extends JavaPlugin {

	final FileConfiguration config = this.getConfig();

	public static HashMap<UUID, ItemStack[]> inventoryStorage;
	public static HashMap<UUID, ItemStack[]> armourStorage;

	public static StaffUtils instance;

	public static StaffUtils getPlugin() {
		return instance;
	}

	public void onEnable() {
		instance = this;

		registerCommands();
		registerListener();

		System.out.println("-*- Staff Utils 2.0 // Made by Nariah");
		System.out.println("-*- For any queries or concerns contact me on skype: sk.ittles");
		System.out.println("-*- Loading Configs");
		loadConfig();
		System.out.println("-*- Success!");

		// PlayerHandler.initialize(this);
		init();
	}

	public void onDisable() {
		saveDefaultConfig();
	}

	private void registerCommands() {
		getCommand("cc").setExecutor(new ClearChat(this));
		getCommand("help").setExecutor(new Help(this));
		getCommand("report").setExecutor(new Report(this));
		getCommand("mutechat").setExecutor(new MuteChat(this));
		getCommand("helpop").setExecutor(new Helpop(this));
		getCommand("h").setExecutor(new H(this));
		getCommand("staffchat").setExecutor(new StaffChat(this));
		getCommand("freeze").setExecutor(new Freeze(this));
		getCommand("list").setExecutor(new List(this));
		getCommand("freezeall").setExecutor(new FreezeAll(this));
		getCommand("alert").setExecutor(new Alert(this));
		getCommand("recover").setExecutor(new InventoryReset(this));
		getCommand("slowchat").setExecutor(new SlowChat(this));
		getCommand("request").setExecutor(new Request(this));
		getCommand("login").setExecutor(new Login(this));

		// Ess
		getCommand("feed").setExecutor(new Feed(this));
		getCommand("fly").setExecutor(new Fly(this));
		getCommand("gamemode").setExecutor(new Gamemode(this));
		getCommand("heal").setExecutor(new Heal(this));
		getCommand("tp").setExecutor(new Teleport(this));
		getCommand("setspawn").setExecutor(new SetSpawn(this));
		getCommand("spawn").setExecutor(new Spawn(this));
		getCommand("kill").setExecutor(new Kill(this));
		getCommand("tphere").setExecutor(new TpHere(this));
		getCommand("vanish").setExecutor(new Vanish(this));
		// Msg
		getCommand("msgall").setExecutor(new MsgAll(this));
	}

	private void registerListener() {
		PluginManager p = Bukkit.getPluginManager();

		p.registerEvents(new Chat(this), this);
		p.registerEvents(new StaffItemEvents(this), this);
		p.registerEvents(new FreezeListener(), this);
		p.registerEvents(new Freeze(this), this);
		p.registerEvents(new StaffNotifications(this), this);
		p.registerEvents(new InventoryReset(this), this);
		p.registerEvents(new Join(this), this);
		p.registerEvents(new SlowChat(this), this);
		p.registerEvents(new LoginListener(this), this);
	}

	protected void init() {
		inventoryStorage = new HashMap<UUID, ItemStack[]>();
		armourStorage = new HashMap<UUID, ItemStack[]>();
	}

	private void loadConfig() {
		this.getConfig().options().copyDefaults(true);
		saveConfig();
	}

}
